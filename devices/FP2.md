---
codename: 'FP2'
name: 'Fairphone 2'
comment: ''
icon: 'phone'
image: 'https://ubports.com/web/image/1559/hanging-fairphone-main1.jpg'
video: 'https://www.youtube.com/embed/a6sIkseTihc'
maturity: .95
---

## The ethical pioneer

Ubuntu Touch on the Fairphone 2: a perfect combination of a secure and open source OS on a sustainable and ethical device. So not only is the Fairphone 2 a modular device, it also improves the users' privacy with Ubuntu Touch. No more 'forced services' that extract your personal and private data and spy on you.

Ubuntu Touch on your Fairphone is a free and open source mobile operating system and is a great alternative to iOS. Ubuntu Touch keeps your Fairphone 2 secure, the OS is virtually free of viruses and other malware that can extract your private data.

Want to know more about the Fairphone 2 and the story behind this device, [read more here](https://ubports.com/devices/fairphone2-environment).
