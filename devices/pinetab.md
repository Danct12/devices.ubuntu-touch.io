---
codename: 'pinetab'
name: 'Pinetab'
comment: 'experimental'
icon: 'tablet-landscape'
noinstall: true
maturity: .1
---

The [Pinetab](https://www.pine64.org/pinetab/) is an affordable ARM-based linux tablet. An [experimental Ubuntu Touch image](https://ci.ubports.com/job/rootfs/job/rootfs-pinetab/) is available.
